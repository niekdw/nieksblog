---
layout: post
title:  "Boek: Crucial Conversations
date:   2019-01-01 09:32:14 +0100
categories: books
---

**De inhoud is slechts een gedeelte van een totaal dat de kwaliteit van een interactie bepaalt.** 

Hoe hoger de bedrevenheid in complexe conversaties, hoe sterker de focus op het niet-inhoudelijke aspect van de interactie.