---
layout: post
title:  "Boek: Are your lights on? door Gerald M. Weinberg"
date:   2019-01-01 09:32:14 +0100
categories: books
---

A quirky little book about the art of problem-solving. Very anecdotal.

## Part 1: What is the problem?
A problem is a difference between things as desired and things as perceived. So a problem can be approached by:

* altering perspective 
* changing current situation
* altering desire

What makes the difference between solving a problem or getting lost is the ability to get a chrystal clear view on the current situation, and the desired situation. If there is anything unclear in each of those states, solving the problem is highly unlikely.

The first reflex when perceiving a problem is to rush to implementing solutions. Dat is een hoogst onefficiënte werkwijze. Fundamenteel is een haarscherp beeld te krijgen van de **huidige situatie** en de **gewenste situatie**.