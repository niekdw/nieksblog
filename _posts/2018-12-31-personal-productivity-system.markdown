---
layout: post
title:  "Personal productivity: processes and tools"
date:   2018-12-31 10:32:14 +0100
categories: weekly_digest
---

My personal system to get things done is as follow. It is a carefully constructed system borrowing from multiple productivity strategies, tweaked to what works best for me. My biggest danger is losing these principles out of sight in favor of switching to the new and shiny. **Avoid at all costs**. 

## Process

* Review MITs for today in task diary 
* Start day by planning schedule in daily planner 
    * Plan daily tasks (process inboxes, review lists, etc)
    * Plan thinking time (30 minute slot with nothing else but general thinking about what is most urgent in that moment - no devices, no connectivity, no distractions)
* Follow schedule as best as possible
* Write out MITs for tomorrow
* Wind down

## Tools of trade 

### Daily planner (A4)

* Plan full day on 30 minute basis. Use as friendly guideline to fall back in, instead of tyrant.

### Tasks and notes book (A5)

* Written todo, watch and later list
    * Rewrite when full, makes sure tasks are fresh and top-of-mind 

### Task diary + inbox (A7)

* At the end of each day write MITs for next day
* Inbox for all thoughts, ideas during the day (which goes to physical inbox)

