---
layout: post
title:  "Weekly digest 1"
date:   2018-12-31 09:32:14 +0100
categories: weekly_digest
---

## Articles I read 

### [Does AI make strong tech companies stronger?](https://www.ben-evans.com/benedictevans/2018/12/19/does-ai-make-strong-tech-companies-stronger)
On why AI - and more specifically machine learning - is likely to make big corporates better in what they already do, instead of enabling them to become a winner-takes-all juggernaut.


* [How to achieve extreme productivity](http://mitsloan.mit.edu/ideas-made-to-matter/how-to-achieve-extreme-productivity)

## Books I read

* [Data Science for Business](https://niekdw.gitlab.io/jekyll/books/2018/12/31/book-data-science-for-business.html)